import {InjectionToken} from '@angular/core';
import {NgxLocalStorageSerializer} from '../interfaces/serializer.interface';

/**
 * Provides an injection token for the services serializer.
 */
export const NGX_LOCAL_STORAGE_SERIALIZER = new InjectionToken<NgxLocalStorageSerializer>('StorageSerializer');
