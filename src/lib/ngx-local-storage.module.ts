import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {NgxLocalStorageDefaultSerializer} from './classes/default-serializer';
import {NGX_LOCAL_STORAGE_SERIALIZER} from './tokens/serializer.token';
import {NGX_LOCAL_STORAGE_CONFIG} from './tokens/config.token';
import {NgxLocalstorageConfiguration} from './interfaces/configuration';
import {NgxLocalStorageDefaultConfig} from './classes/default-config';


@NgModule({
    imports: [],
    declarations: [],
    exports: [],
    providers: [
        {
            provide: NGX_LOCAL_STORAGE_SERIALIZER,
            useClass: NgxLocalStorageDefaultSerializer
        }
    ]
})
export class NgxLocalStorageModule {

    /**
     * Initializes the module for the root module with the given configuration.
     */
    public static forRoot(config?: NgxLocalstorageConfiguration): ModuleWithProviders<NgxLocalStorageModule> {
        return {
            ngModule: NgxLocalStorageModule,
            providers: [
                {
                    provide: NGX_LOCAL_STORAGE_CONFIG,
                    useValue: Object.assign(NgxLocalStorageDefaultConfig, config)
                }
            ]
        };
    }

    /**
     * Creates a new instance.
     */
    constructor(@Optional() @SkipSelf() parentModule: NgxLocalStorageModule) {
        if (parentModule) {
            throw new Error('NgxLocalStorageModule is already loaded. Import it in the AppModule only');
        }
    }
}
