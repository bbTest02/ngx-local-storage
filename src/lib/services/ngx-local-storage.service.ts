import {Injectable, Inject} from '@angular/core';

import {NgxLocalstorageConfiguration} from '../interfaces/configuration';
import {NGX_LOCAL_STORAGE_CONFIG} from '../tokens/config.token';
import {NGX_LOCAL_STORAGE_SERIALIZER} from '../tokens/serializer.token';
import {NgxLocalStorageSerializer} from '../interfaces/serializer.interface';

/**
 * Provides a service to access the localstorage.
 */
@Injectable({providedIn: 'root'})
export class NgxLocalStorageService {

    /**
     * Creates a new instance.
     */
    constructor(
        @Inject(NGX_LOCAL_STORAGE_SERIALIZER) private readonly defaultSerializer: NgxLocalStorageSerializer,
        @Inject(NGX_LOCAL_STORAGE_CONFIG) public readonly config: NgxLocalstorageConfiguration
    ) {
    }

    /**
     * Gets the number of entries in the applications local storage.
     */
    public count(): number | undefined {
        try {
            return localStorage.length;
        } catch (error) {
            console.error(error);
        }
    }

    /**
     * Returns the nth (defined by the index parameter) key in the storage.
     * The order of keys is user-agent defined, so you should not rely on it.
     * @param index   An integer representing the number of the key you want to get the name of. This is a zero-based index.
     */
    public getKey(index: number): string | null | undefined {
        if (index < 0) {
            console.error(new Error('index has to be 0 or greater'));
        }
        try {
            return localStorage.key(index);
        } catch (error) {
            console.error(error);
        }
    }

    /**
     * Adds the value with the given key or updates an existing entry.
     * @param key     Key to store.
     * @param value   Value to store.
     * @param prefixOrSerializer  Optional prefix or serializer to overwrite the configured one.
     */
    public set(key: string, value: any, prefixOrSerializer?: string | NgxLocalStorageSerializer): void;
    /**
     * Adds the value with the given key or updates an existing entry.
     * @param key     Key to store.
     * @param value   Value to store.
     * @param prefixOrSerializer  prefix or serializer to overwrite the configured one.
     */
    public set(key: string, value: any, prefixOrSerializer: string | NgxLocalStorageSerializer): void;
    /**
     * Adds the value with the given key or updates an existing entry.
     * @param key     Key to store.
     * @param value   Value to store.
     * @param prefix  Optional prefix to overwrite the configured one.
     * @param serializer  Optional serilizer.
     */
    public set(key: string, value: any, prefix: string, serializer: NgxLocalStorageSerializer): void;
    /**
     * Adds the value with the given key or updates an existing entry.
     * @param key     Key to store.
     * @param value   Value to store.
     * @param prefixOrSerializer  Optional prefix or serializer to overwrite the configured one.
     * @param serializer  Optional serilizer.
     */
    public set(key: string, value: any, prefixOrSerializer?: string | NgxLocalStorageSerializer, serializer?: NgxLocalStorageSerializer): void {

        const prefix = typeof prefixOrSerializer === 'string' ? prefixOrSerializer : undefined;
        serializer = this.isSerializer(prefixOrSerializer)
            ? (prefixOrSerializer as NgxLocalStorageSerializer)
            : !!serializer
                ? serializer
                : this.defaultSerializer;

        if (
            this.config.allowNull ||
            (!this.config.allowNull &&
                value !== 'null' &&
                value !== null &&
                value !== undefined)
        ) {
            localStorage.setItem(this.constructKey(key, prefix, this.config.prefix), serializer.serialize(value));
        } else {
            this.remove(key, this.constructKey(key, prefix, this.config.prefix));
        }
    }

    /**
     * Gets the entry specified by the given key or null.
     * @param key     Key identifying the wanted entry.
     * @param prefixOrSerializer  Optional prefix or serializer to overwrite the configured one.
     * @param serializer  Optional serilizer.
     */
    public get(key: string, prefixOrSerializer?: string | NgxLocalStorageSerializer): any | null | undefined;
    /**
     * Gets the entry specified by the given key or null.
     * @param key     Key identifying the wanted entry.
     * @param prefixOrSerializer  prefix or serializer to overwrite the configured one.
     */
    public get(key: string, prefixOrSerializer: string | NgxLocalStorageSerializer): any | null | undefined;
    /**
     * Gets the entry specified by the given key or null.
     * @param key     Key identifying the wanted entry.
     * @param prefix  prefix or serializer to overwrite the configured one.
     * @param serializer serilizer.
     */
    public get(key: string, prefix: string, serializer: NgxLocalStorageSerializer): any | null | undefined;
    /**
     * Gets the entry specified by the given key or null.
     * @param key     Key identifying the wanted entry.
     * @param prefixOrSerializer  Optional prefix or serializer to overwrite the configured one.
     * @param serializer  Optional serilizer.
     */
    public get(key: string, prefixOrSerializer?: string | NgxLocalStorageSerializer, serializer?: NgxLocalStorageSerializer): any | null | undefined {

        const prefix = typeof prefixOrSerializer === 'string' ? prefixOrSerializer : undefined;
        serializer = this.isSerializer(prefixOrSerializer)
            ? (prefixOrSerializer as NgxLocalStorageSerializer)
            : !!serializer
                ? serializer
                : this.defaultSerializer;

        try {
            return serializer.deserialize(localStorage.getItem(this.constructKey(key, prefix, this.config.prefix)));
        } catch (error) {
            console.error(error);
        }
    }

    /**
     * Removes the entry specified by the given key.
     * @param key     Key identifying the entry to remove.
     * @param prefix  Optional prefix to overwrite the configured one.
     */
    public remove(key: string, prefix?: string): void {
        try {
            localStorage.removeItem(this.constructKey(key, prefix, this.config.prefix));
        } catch (error) {
            console.error(error);
        }
    }

    /**
     * Clears all entries of the applications local storage.
     */
    public clear(): void {
        try {
            localStorage.clear();
        } catch (error) {
            console.error(error);
        }
    }

    /**
     * Constructs the storage key based on a prefix - if given - and the key itself
     */
    private constructKey(key: string, prefix?: string, configuredPrefix?: string): string {
        const prefixToUse = prefix || configuredPrefix;
        if (prefixToUse) {
            return `${prefixToUse}_${key}`;
        }
        return key;
    }

    /**
     * StorageSerializer Guard
     */
    private isSerializer(prefixOrSerializer: string | NgxLocalStorageSerializer): prefixOrSerializer is NgxLocalStorageSerializer {
        return !!prefixOrSerializer && (prefixOrSerializer as NgxLocalStorageSerializer).serialize !== undefined;
    }
}
