import {NgxLocalStorageSerializer} from './interfaces/serializer.interface';

/**
 * Constructs the storage key based on a prefix - if given - and the key itself
 */
export const constructKey = (key: string, prefix?: string, configuredPrefix?: string): string => {
    const prefixToUse = prefix || configuredPrefix;
    if (prefixToUse) {
        return `${prefixToUse}_${key}`;
    }
    return key;
};

/**
 * StorageSerializer Guard
 */
export const isSerializer = (prefixOrSerializer: string | NgxLocalStorageSerializer): prefixOrSerializer is NgxLocalStorageSerializer => {
    return !!prefixOrSerializer && (prefixOrSerializer as NgxLocalStorageSerializer).serialize !== undefined;
};
