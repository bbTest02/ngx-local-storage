import {NgxLocalstorageConfiguration} from '../interfaces/configuration';

/**
 * Provides a default configuration
 */
export const NgxLocalStorageDefaultConfig: NgxLocalstorageConfiguration = {
    allowNull: true
};
