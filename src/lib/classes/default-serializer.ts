import {NgxLocalStorageSerializer} from '../interfaces/serializer.interface';

/**
 * Provides a default serialization mechanism while
 */
export class NgxLocalStorageDefaultSerializer implements NgxLocalStorageSerializer {
    /**
     * @inheritdoc
     */
    public serialize(value: any): string {
        return JSON.stringify(value);
    }

    /**
     * @inheritdoc
     */
    public deserialize(storedValue: string): any {
        return JSON.parse(storedValue);
    }
}
